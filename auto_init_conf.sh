#!/bin/bash

set -e

timestamp() {
  date +"%s"
}

echo "##########################################################"
echo "/!\  PUT THIS SCRIPT AT THE ROOT OF THE APPLICATION   /!\ "
echo "##########################################################"
echo ""

    echo ""
    echo "####################################"
    echo "FINDING PATH TO THIS SCRIPT"
    echo "####################################"
    LOCATION="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/"
    cd $LOCATION
    
    echo ""
    echo $LOCATION
    echo ""
    echo ""


    echo "###################################"
    echo "INITIALIZING CONFIGURATION"
    echo "###################################"
    echo ""
    echo "<?php // AUTO-GENERATED PATH FILE" > config_path.php

    echo "################################"
    echo "GENERATING CONFIGURATION"
    echo "################################"
    echo ""
    echo '$z = strlen($_SERVER["DOCUMENT_ROOT"]);' >> config_path.php

    find `pwd` ! -iname '*.class' | while read line ; do

        var=$(awk '{print $1}' <<< "$line")
        NOM_FICHIER=`echo $var | awk -F "[/]" 'BEGIN{OFS="/";} {print $NF}'`
        EXTENSION=`basename $NOM_FICHIER | awk -F "[.]" 'BEGIN{OFS="/";} {print $NF}'`
        
        #WEB PATH CONFIGUTATION (FOR CLIENT SIDE PATH)

        WEB_PATH_DOC='$_SERVER["DOCUMENT_ROOT"]'
        WEB_PATH_FILE="/${var/$LOCATION/}";
        if [ $var"/" == $LOCATION ]; then WEB_PATH_FILE="/""${PWD##*/}"; fi
        LIGNE='$configWeb["'$NOM_FICHIER'"] = substr("'$var'", $z);'
        echo $LIGNE >> config_path.php
        
        #ABSOLUTE PATH CONFIGURATION (FOR SERVER SIDE)

        LIGNE='$config["'$NOM_FICHIER'"] = "'$line'";'
        echo $LIGNE >> config_path.php


    done

    echo "?>" >> config_path.php    

    echo "########################"
    echo "GENERATING HTACCESS"
    echo "########################"
    echo ""

    file=.htaccess
    
    if [ -e "$file" ]; then
        echo ""
        echo "HTACCESS FILE ALREADY EXIST"
        echo ""
        echo "BACKING UP OLD FILE"
        mv .htaccess .htaccess.backup_$(timestamp)
    fi 

        CONFIG_PATH=`find $LOCATION -name 'config_path.php'`
        echo 'php_value auto_prepend_file "'$CONFIG_PATH'"' > .htaccess
    

    echo ""
    echo "#######################################"
    echo "CONFIGURATION GENERATED SUCCESSFULLY"
    echo "#######################################"
    echo ""



